<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Gatsbookmarks, show your bookmarks to the world !
</h1>

Add your bookmarks, then let Gatsby index them (using a internal search engine) and display them by tags or with a fulltext search !

## Roadmap
DONE:
- Search engine with Lunr
- Tag page

TO DO:
- Create a tag cloud component, to fill the home
- Maybe add a last added items under it on the front page ?
- Think about PWA
- Look for publication on ActivityPub

## Usage
### Add a bookmark
```
npm run bookmark
npm run b
```
You can add multiple tags with commas: `HTML,accessibility,HTML5`

### Index
```
npm run build
```

## Options
In `gatsby-config.js`, edit the metadatas:
```
siteMetadata: {
	highlightResults: true|false, // Boolean, if you want to highlight the searched text in the results.
}
```

# Thanks
- Lunr Search: https://assortment.io/posts/gatsby-site-search-lunr-js
