module.exports = {
  siteMetadata: {
    title: `Gatsbookmarks Starter`,
    description: `Look at my bookmarks, they are beautiful !`,
    author: `@kokolascode`,
		highlightResults: false,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `bookmarks`,
				path: `${__dirname}/bookmarks`,
			},
		},
		`gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
		`gatsby-transformer-remark`,
		{
			resolve: 'gatsby-plugin-lunr',
			options: {
				languages: [{ name: 'en' }],
				fields: [
					{ name: 'title', store: true, attributes: { boost: 10 } },
					{ name: 'tags', store: true, attributes: { boost: 20 } },
					{ name: 'description', store: true },
					{ name: "date", store: true },
					{ name: "link", store: true },
				],
				resolvers: {
					MarkdownRemark: {
						title: node => node.frontmatter.title,
						tags: node => node.frontmatter.tags,
						description: node => node.rawMarkdownBody,
						date: node => node.frontmatter.date,
						link: node => node.frontmatter.link,
					}
				},
				filename: 'search_index.json',
			}
		}
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
