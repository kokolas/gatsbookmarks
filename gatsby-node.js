/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, actions }) => {
	const { createPage } = actions

	return Promise.all([
		declareTagPages(graphql, createPage),
	])
}

exports.onCreateNode = ({ node, actions, getNode }) => {
	const { createNodeField } = actions

	if (node.internal.type === `MarkdownRemark`) {
		const value = createFilePath({ node, getNode })
		createNodeField({
			name: `slug`,
			node,
			value,
		})
	}
}

const declareTagPages = (graphql, createPage) => {
	const tagTemplate = path.resolve(`./src/templates/tag.js`)
	return graphql(
		`
      {
        allMarkdownRemark(
          filter: {
            frontmatter: { type: { eq: "Bookmark" } }
          }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                tags
              }
            }
          }
        }
      }
    `
	).then(result => {
		if (result.errors) {
			throw result.errors
		}

		// Create tag pages.
		const tags = {}
		result.data.allMarkdownRemark.edges.forEach(edge => {
			edge.node.frontmatter.tags.forEach(tag => {
				if (!tags[tag]) {
					tags[tag] = []
				}
				tags[tag].push(edge.node.frontmatter.path)
			})
		})
		Object.keys(tags).forEach(tag => {
			createPage({
				path: `/tag/${tag}`,
				component: tagTemplate,
				context: {
					tag: tag,
				},
			})
		})
	})
}
