const moment = require('moment')

module.exports = function(plop) {
    const year = moment().format('YYYY')
    const monthAndDay = moment().format('MM-DD')

    plop.setPartial('date', `${moment().format('YYYY-MM-DDTHH:mm:ssZ')}`)

    plop.setHelper('tags', function (tagsStr) {
        // Split and create the tag list from the 'tag1,tag2' string entered in plop
    		const tags = tagsStr.split(',')
        const tagsLines = tags.reduce((acc, tag) => `${acc}  - ${tag}\n`, '')
        return `tags:\n${tagsLines}`;
    });

    plop.setGenerator('bookmark', {
        description: 'create a bookmark',
        prompts: [
            {
                type: 'input',
                name: 'url',
                message: 'url',
            },
            {
                type: 'input',
                name: 'name',
                message: 'name',
            },
            {
                type: 'input',
                name: 'tags',
                message: 'tags',
            },
            {
                type: 'input',
                name: 'description',
                message: 'description',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `bookmarks/${year}/${monthAndDay} - {{name}}.md`,
                templateFile: 'plop-templates/bookmark.md',
            },
        ],
    })
}
