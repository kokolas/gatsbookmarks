import React, { Fragment } from "react"
import PropTypes from 'prop-types'
import Tag from './tag'
import Highlighter from "react-highlight-words"

const Result = ({ title, link, date, description, tags, query, highlight }) => (<div style={{marginBottom: '17px'}}>
		<h3 className="search-results-list__heading" style={{marginBottom: '0'}}>
			<a href={link} className="search-results-list__link" target="_blank" rel="noopener noreferrer">
				{
					highlight ?
						<Highlighter
							searchWords={[query]}
							autoEscape={true}
							textToHighlight={title}
						/> : <span>{title}</span>
				}
			</a>
			{tags && <Fragment>
				&nbsp;
				{tags.map(tag => <Tag key={tag} value={tag}/>)}
			</Fragment>}
		</h3>
		<small>{new Date(date).toLocaleString("en-US")}</small>
		{description && (
			<p>
				{
					highlight ?
						<Highlighter
							searchWords={[query]}
							autoEscape={true}
							textToHighlight={description}
						/> : <span
							dangerouslySetInnerHTML={{
								__html: description,
							}}
							style={{ p: { display: 'inline' } }}
						/>
				}
			</p>
		)}
	</div>
)

Result.propTypes = {
	title: PropTypes.string,
	link: PropTypes.string,
	date: PropTypes.object,
	description: PropTypes.string,
	tags: PropTypes.array,
	query: PropTypes.string,
	highlight: PropTypes.bool
}

Result.defaultProps = {
	highlight: true
}


export default Result
