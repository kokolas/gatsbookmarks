import React from "react"
import PropTypes from "prop-types"
import Result from './result'

const SearchResults = ({ query, results, highlight }) => (
	<section aria-label="Search results for all posts">
		{!!results.length && query && (
			<h2
				className="search-results-count"
				id="search-results-count"
				aria-live="assertive"
			>
				Found {results.length} posts on "{query}"
			</h2>
		)}
		{!!results.length && (
			<ol className="search-results-list">
				{results.map(({ title, link, date, description, tags }) => (
					<li key={title}>
						<Result
							title={title}
							link={link}
							date={date}
							description={description}
							tags={tags}
							query={query}
							highlight={highlight}
						/>
					</li>
				))}
			</ol>
		)}
	</section>
)

SearchResults.propTypes = {
	query: PropTypes.string,
	results: PropTypes.array,
	highlight: PropTypes.bool
}

SearchResults.defaultProps = {
	highlight: true
}

export default SearchResults
