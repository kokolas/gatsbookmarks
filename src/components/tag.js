import React, { Fragment } from "react"
import PropTypes from 'prop-types'
import { Link } from "gatsby"
import {colorsForText} from "../utils"

const Tag = ({value}) => {
	const colors = colorsForText(value)
	return <Fragment>
		<Link
			to={`/tag/${value}`}
			style={{
				color: `white`,
				textDecoration: `none`,
			}}
		>
			<span style={{backgroundColor: colors.backgroundColor, color: colors.textColor, fontSize: 'medium'}}>
				{`#${value}`}
			</span>
		</Link>
		&nbsp;
	</Fragment>
}

Tag.propTypes = {
	value: PropTypes.string
}

export default Tag
