import React, { useState, useEffect } from "react"
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import SearchForm from "../components/searchForm"
import SearchResults from "../components/searchResults"

/**
 * Debounce. Blocks consecutive calls at <func> during <wait>, then trigger the <func>
 * @param func the function to call
 * @param wait
 * @param immediate
 * @returns {Function}
 */
const debounce = (func, wait, immediate) => {
	let timeout
	return function (...args) {
		const context = this
		const later = function () {
			timeout = null
			if (!immediate) func.apply(context, args)
		}
		const callNow = immediate && !timeout
		clearTimeout(timeout)
		timeout = setTimeout(later, wait)
		if (callNow) func.apply(context, args)
	}
}


const Index = ({ data, location }) => {
	const [results, setResults] = useState([])
	const searchQuery = new URLSearchParams(location.search).get("keywords") || ""

	useEffect(() => {
		if (searchQuery && window.__LUNR__) {
			const debouncedSearch = debounce(async () => {
				const lunr = await window.__LUNR__.__loaded
				const refs = lunr.en.index.search(searchQuery)
				const posts = refs.map(({ ref }) => lunr.en.store[ref])

				setResults(posts)
			}, 500)

			debouncedSearch()
		}

		if (!searchQuery) setResults([])
	}, [location.search])

	return (
		<Layout location={location} title={data.site.siteMetadata.title}>
			<SearchForm query={searchQuery} />
			<SearchResults query={searchQuery} results={results} highlight={data.site.siteMetadata.highlightResults}/>
		</Layout>
	)
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        highlightResults
      }
    }
  }
`
