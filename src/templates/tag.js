import React from 'react'
import get from 'lodash/get'
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import {colorsForText} from "../utils"
import Result from '../components/result'

class TagPageTemplate extends React.Component {
	render() {
		const currentTag = get(this.props, 'pageContext.tag')
		const siteTitle = get(this.props, 'data.site.siteMetadata.title')
		const bookmarks = get(this, 'props.data.bookmarks.edges')
		const colors = colorsForText(currentTag)

		return (
			<Layout location={this.props.location} title={siteTitle}>
				<h1 style={{backgroundColor: colors.backgroundColor, color: colors.textColor, textAlign: 'center'}}>{currentTag}</h1>
				<div>
					<h2>Bookmarks</h2>
					<ul>{bookmarks.map(bookmark =>
						<li
							key={bookmark.node.frontmatter.title}
							style={{
								marginBottom: '15px',
							}}
						>
							<Result
								title={bookmark.node.frontmatter.title}
								link={bookmark.node.frontmatter.link}
								date={bookmark.node.frontmatter.date}
								description={bookmark.node.html}
								tags={bookmark.node.frontmatter.tags}
								query={currentTag}
								highlight={this.props.data.site.siteMetadata.highlightResults}
							/>
						</li>)}
					</ul>
				</div>
			</Layout>
		)
	}
}

export default TagPageTemplate

export const pageQuery = graphql`
    query ByTag($tag: String!) {
        site {
            siteMetadata {
                title
                author
                highlightResults
            }
        }
        bookmarks: allMarkdownRemark(
            filter: {
                frontmatter: { tags: { in: [$tag] } }
            }
            sort: { fields: [frontmatter___date], order: DESC }
        ) {
            edges {
                node {
                    id
                    html
                    frontmatter {
                        title
                        link
                        date(locale: "en-US")
                        tags
                    }
                }
            }
        }
    }
`
