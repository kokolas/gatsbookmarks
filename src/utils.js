const palette = [
	['#fbc9a0', 'black'],
	['#c3d9f2', 'black'],
	['#d2ecd1', 'black'],
	['#ffc2c2', 'black'],
	['#fdf4bf', 'black'],
]

// One text should always resolve to the same color
const cache = {}

const getRandomInt = max => {
	return Math.floor(Math.random() * Math.floor(max));
}

const colorsForText = text => {
	if(cache[text]) {
		return cache[text]
	}

	const color = palette[getRandomInt(palette.length)]
	cache[text] = {
		backgroundColor: color[0],
		textColor: color[1]
	}
	return cache[text]
}

export {
	colorsForText
}
